package com.ealanta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * @author davidhay
 */
@SpringBootApplication
public class UsertasksRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsertasksRestApplication.class, args);
	}

	public static class Config {

		@Bean
		public javax.validation.Validator localValidatorFactoryBean() {
			return new LocalValidatorFactoryBean();
		}
	}

}
