package com.ealanta.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ealanta.domain.Task;

/**
 * @author davidhay
 */
@RepositoryRestResource(collectionResourceRel = "tasks", path = "tasks")
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

	List<Task> findByUserUsernameIgnoringCase(@Param("username") String username);

}
