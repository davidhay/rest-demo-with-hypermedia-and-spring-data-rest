package com.ealanta.domain;

/**
 * @author davidhay
 */
public enum Priority {
	LO,
	MED,
	HI
}
