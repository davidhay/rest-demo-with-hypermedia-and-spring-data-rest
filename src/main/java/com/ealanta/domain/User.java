package com.ealanta.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="user")
public class User extends AbstractBaseEntity {

	private static final long serialVersionUID = 1L;

	@Column(name="first",length=80)
	@Size(min=2,max=80)
	@NotBlank
	private String firstname;
	
	@Column(name="last",length=80)
	@Size(min=2,max=80)
	@NotBlank
	private String lastname;
	
	@Column(name="username", length=20, unique=true)
	@Size(min=5,max=20)
	@NotBlank
	private String username;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="user")
	private List<Task> tasks = new ArrayList<Task>();

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public void addTasks(Task... tasks) {
		if(tasks == null){
			return;
		}
		for(Task task : tasks){
			this.tasks.add(task);
			task.setUser(this);
		}
	}
	
}
