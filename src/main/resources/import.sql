insert into user(username,first,last,creation_ts,update_ts,version,id) values ('joebloggs','Joe','Bloggs',now(),null,0,1)
insert into user(username,first,last,creation_ts,update_ts,version,id) values ('molliem','Mollie','Malone',now(),null,0,2)

insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task eleven'   ,1   ,'LO'    ,now()  ,11,0      ,now()      ,null     ,'N')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twelve'   ,1   ,'MED'   ,now()  ,12,0      ,now()      ,null     ,'Y')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task thirteen' ,1   ,'HI'    ,now()  ,13,0      ,now()      ,null     ,'N')

insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twenty one'   ,2   ,'LO'    ,'2010-01-01 00:00:00'  ,21, 0      ,now()      ,null     ,'N')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twenty two'   ,2   ,'MED'   ,'2010-01-01 00:00:00'  ,22, 0      ,now()      ,null     ,'Y')
insert into user_task(description,user_id,priority,due_date, id   ,version,creation_ts,update_ts,completed) values ('task twenty three' ,2   ,'HI'    ,'2010-01-01 00:00:00'  ,23, 0      ,now()      ,null     ,'N')

