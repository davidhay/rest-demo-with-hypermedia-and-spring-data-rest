package com.ealanta.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.ealanta.UsertasksRestApplication;

/**
 * @author davidhay
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UsertasksRestApplication.class)
@WebAppConfiguration
public class RestUserTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}
	
	@Test
	public void testApi() throws Exception {
		mockMvc.perform(get("/api"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$._links.users.href", is(equalTo("http://localhost/api/users{?page,size,sort}"))))
		.andExpect(jsonPath("$._links.users.templated", is(equalTo(true))))
		.andExpect(jsonPath("$._links.tasks.href", is(equalTo("http://localhost/api/tasks{?page,size,sort}"))))
		.andExpect(jsonPath("$._links.tasks.templated", is(equalTo(true))))
		.andExpect(jsonPath("$._links.profile.href", is(equalTo("http://localhost/api/alps"))))
		.andExpect(status().isOk());
	}

	@Test
	public void testGetUser1() throws Exception {
		mockMvc.perform(get("/api/users/1"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.username", is(equalTo("joebloggs"))))
		.andExpect(jsonPath("$.firstname",is(equalTo("Joe"))))
		.andExpect(jsonPath("$.lastname", is(equalTo("Bloggs"))))
		.andExpect(jsonPath("$._links.self.href", is(equalTo("http://localhost/api/users/1"))))
		.andExpect(jsonPath("$._links.tasks.href", is(equalTo("http://localhost/api/users/1/tasks"))))
		.andExpect(status().isOk());
	}

	@Test
	public void testCreateUser() throws Exception {
		String userJson = "{\"firstname\" : \"santa\",\"lastname\" : \"Saint\", \"username\" : \"Nicholas\"}";
		mockMvc.perform(post("/api/users")
				.contentType(MediaType.APPLICATION_JSON)
                .content(userJson))
		.andDo(print())
		.andExpect(header().string("Location", is(equalTo("http://localhost/api/users/3"))))
		.andExpect(status().isCreated());
	}
	@Test
	public void testCreateUserTask() throws Exception {
		String userTaskJson = "{\"description\":\"desc\",\"completed\":false,\"dueDate\":\"2015-12-31T23:00:00\",\"priority\":\"HI\",\"user\":\"http://localhost/api/users/1\"}";
		mockMvc.perform(post("/api/tasks")
				.contentType(MediaType.APPLICATION_JSON)
                .content(userTaskJson))
		.andDo(print())
		.andExpect(header().string("Location", is(equalTo("http://localhost/api/tasks/24"))))
		.andExpect(status().isCreated());
	}

	
	@Test
	public void testGetUser1Task() throws Exception {
		mockMvc.perform(get("/api/users/2/tasks/21"))
		.andDo(print())
		.andExpect(content().contentType("application/hal+json"))
		.andExpect(jsonPath("$.description", is(equalTo("task twenty one"))))
		.andExpect(jsonPath("$.completed",is(equalTo(false))))
		.andExpect(jsonPath("$.dueDate", is(equalTo("2010-01-01T00:00:00.000+0000"))))
		.andExpect(jsonPath("$._links.self.href", is(equalTo("http://localhost/api/tasks/21"))))
		.andExpect(jsonPath("$._links.user.href", is(equalTo("http://localhost/api/tasks/21/user"))))
		.andExpect(status().isOk());
	}
}
