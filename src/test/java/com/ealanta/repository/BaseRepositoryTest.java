package com.ealanta.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.ealanta.UsertasksRestApplication;

/**
 * @author davidhay
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UsertasksRestApplication.class)
@Transactional
public abstract class BaseRepositoryTest {
	
	@PersistenceContext
	protected  EntityManager em;
	
	@Autowired
	protected JdbcTemplate template;
	
	@Before
	public void setup() {
		Assert.assertNotNull(em);
		Assert.assertNotNull(template);
		Assert.assertEquals(2, JdbcTestUtils.countRowsInTable(template, "user"));
		Assert.assertEquals(6, JdbcTestUtils.countRowsInTable(template, "user_task"));
	}

}
