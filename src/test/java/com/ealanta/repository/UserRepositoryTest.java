package com.ealanta.repository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.ealanta.UsertasksRestApplication;
import com.ealanta.domain.Priority;
import com.ealanta.domain.Task;
import com.ealanta.domain.User;

/**
 * @author davidhay
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UsertasksRestApplication.class)
@Transactional
public class UserRepositoryTest extends BaseRepositoryTest {

	@Autowired
	private UserRepository repo;

	@Before
	public void setup() {
		Assert.assertNotNull(repo);
	}

	/**
	 * Tests that when we save a new user (with no tasks), it is given an id and
	 * can be looked up by id.
	 */
	@Test
	public void testSaveUserOnly() {

		User user = new User();
		user.setFirstname("Bruce");
		user.setLastname("Wayne");
		user.setUsername("batman");
		User user2 = repo.save(user);
		Assert.assertEquals(user, user2);

		em.flush();
		Assert.assertNotNull(user.getId());

		User user3 = repo.findByUsernameAllIgnoringCase("batman");
		Assert.assertEquals(user, user3);

		Assert.assertEquals(3, JdbcTestUtils.countRowsInTable(template, "user"));
	}

	/**
	 * Tests that when we save a new user (with no tasks), it is given an id and
	 * can be looked up by id.
	 */
	@Test
	public void testSaveUserWithTasks() {

		User user = new User();
		user.setFirstname("Bill");
		user.setLastname("Smiith");
		user.setUsername("bsmith");

		Task t1 = new Task();
		t1.setDescription("task M");
		t1.setPriority(Priority.MED);

		Task t2 = new Task();
		t2.setDescription("task H");
		t2.setPriority(Priority.HI);

		Task t3 = new Task();
		t3.setDescription("task L");
		t3.setPriority(Priority.LO);

		user.addTasks(t1, t2, t3);

		User user2 = repo.save(user);
		Assert.assertEquals(user, user2);

		em.flush();
		Long id = user.getId();
		em.clear();

		User user3 = repo.findByUsernameAllIgnoringCase("bsmith");
		Assert.assertEquals(id, user3.getId());
		Assert.assertEquals(3, user3.getTasks().size());
		Assert.assertEquals(3, JdbcTestUtils.countRowsInTable(template, "user"));
		Assert.assertEquals(9, JdbcTestUtils.countRowsInTable(template, "user_task"));

	}

	@Test
	public void testValidation() {
		try {
			User user = new User();
			user.setFirstname("sml");
			user.setLastname("sml");
			user.setUsername("sml");
			User user2 = repo.save(user);

			Assert.assertEquals(user, user2);

			em.flush();
			Assert.fail("exception expected");
		} catch (Exception ex) {
			Assert.assertTrue(ex instanceof ConstraintViolationException);
			ConstraintViolationException cve = (ConstraintViolationException) ex;
			Assert.assertTrue(cve.getMessage().indexOf("size must be between 5 and 20', propertyPath=username") >= 0);
		}

	}
	
	@Test
	public void testUser1(){
		User user = this.repo.findOne(1L);
		Set<Long> taskIds = user.getTasks().stream().map(t -> t.getId()).collect(Collectors.toSet());
		Set<Long> expectedIds = new HashSet<Long>(Arrays.asList(11L,12L,13L));
		Assert.assertEquals(expectedIds, taskIds);
	}
}
