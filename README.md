# Rest Demo with Spring Boot and Spring Data Rest #

This Java Application also creates a Rest API for Users/Tasks. This app uses SpringBoot/SpringDataRest with HSQLDB and supports Hypermedia.
The rest endpoints have integration tests written using Spring's MockMVC. (It's possible to use Spring's MockMVC together with Cucumber for BDD acceptance testing).

## REST URLS FOR READ/GET ##

* http://localhost:8080/api
* http://localhost:8080/api/users
* http://localhost:8080/api/users/{userid}
* http://localhost:8080/api/users/{userid}/tasks
* http://localhost:8080/api/users/{userid}/tasks/{taskid}
* http://localhost:8080/api/tasks
* http://localhost:8080/api/tasks/{taskid}
* http://localhost:8080/api/tasks/{taskid}/user

## REST URLS POST/CREATE ##

to create a new user 

post to http://localhost:8080/api/users

```
#!python

curl -i -X POST -H "Content-Type: application/json" -d '{"firstname" : "santa","lastname" : "Saint", "username" : "Nicholas"}' http://localhost:8080/api/users

HTTP/1.1 201 Created

Location: http://localhost:8080/api/users/4
```



to create a new user task

post to http://localhost:8080/api/tasks/


```
#!python

curl -i -X POST -H "Content-Type: application/json" \

-d '{"description":"desc","completed":false,"dueDate":"2015-12-31T23:00:00","priority":"HI","user":"http://localhost:8080/api/users/1"}'http://localhost:8080/api/tasks

HTTP/1.1 201 Created

Server: Apache-Coyote/1.1

Location: http://localhost:8080/api/tasks/24
```


To run the java app


```
#!python

$ mvn spring-boot:run
```